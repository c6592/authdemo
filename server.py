import base64
import binascii
import hashlib
import hmac
import json
import wikipedia
from typing import Optional
from fastapi import FastAPI, Form, Cookie, Body
from fastapi.responses import Response
from random import choice


users = {
    "admiralbenbou@bk.ru": {
        "name": "Mitrich",
        "password": "cbeb52e1ec9cdfcaa8c724bf26d82133c57bcc92436325c959f93c0d3cac8208",
        "balance": 60_000
    },
    "misterkrabs@mail.ru": {
        "name": "MisterKrabs",
        "password": "40988531f121684acd3d63105013bf13703790a3954cfc1e6712feac493d7da7",
        "balance": 600_000
    }
}


app = FastAPI()

SECRET_KEY = "a924da7e158858d88fa49c203a96806a5366013a4fc76cca7edc1eaa918344c1"
PASSWORD_SALT = "eb28838e1c2b02072c4d7dbc61e61d62f81b134578aa22b92d98f74f455269c5"

def sign_data(data: str) -> str:
    return hmac.new(
        SECRET_KEY.encode(),
        msg = data.encode(),
        digestmod = hashlib.sha256
    ).hexdigest().upper()

def get_login_from_signed_string(login_signed: str) -> Optional[str]:
    if "." not in login_signed:
        return None
    login_base64, sign = login_signed.split(".")
    try:
        login = base64.b64decode(login_base64.encode()).decode()
    except binascii.Error:
        return None
    valid_sign = sign_data(login)
    """Makes a new signature(hash) from received login"""
    if hmac.compare_digest(valid_sign, sign):
        """Returns True if a new hash is equal received hash"""
        return login

def verify_password(login: str, password: str) -> bool:
    """Compares inputed password with stored password and returns True if they are the same"""
    password_hash = hashlib.sha256((password + PASSWORD_SALT).encode()).hexdigest().lower()
    stored_password_hash = users[login]["password"].lower()
    # print(type(password_hash), type(stored_password_hash))
    return password_hash == stored_password_hash

@app.get("/")
def index_page(login: Optional[str] = Cookie(default=None)):
    with open("templates/login.html", "r") as f:
        login_page = f.read()
    if not login:
        """If there is not any login, return start page"""
        return Response(login_page, media_type="text/html")
    valid_login = get_login_from_signed_string(login)
    if not valid_login:
        response = Response(login_page, media_type="text/html")
        response.delete_cookie(key="login")
        return response

    try:
        user = users[valid_login]
    except KeyError:
        response = Response(login_page, media_type="text/html")
        response.delete_cookie(key="login")
        return response
    return Response(f"Hello, {users[valid_login]['name']}", media_type="text/html")


@app.post("/login")
# def process_login_page(login: str = Form(...), password: str = Form(...))
def process_login_page(data: dict = Body(...)):
    login = data["login"]
    password = data["password"]
    user = users.get(login)
    if not user or not verify_password(login, password):
        return Response(
            json.dumps({
                "success": False,
                "message": "Fuck off."
            }),
            media_type="application/json")

    login_signed = base64.b64encode(login.encode()).decode() + "." + sign_data(login)

    response = Response(
        json.dumps({
            "success": True,
            "message": f"Your login: {login} <br /> Balance: {users[login]['balance']}"
        }),
        media_type="application/json")
    response.set_cookie(key="login", value=login_signed)
    return response


def check_phone(phone):
    """Remove unacceptable symbols from inputed phone"""
    new_phone = ""
    for char in phone:
        if char.isnumeric():
            new_phone += char
    if len(new_phone) == 11 and (new_phone[:2] == "79" or new_phone[:2] == "89"):
        new_phone = new_phone[2:]
    elif len(new_phone) == 10 and new_phone[0] == "9":
        new_phone = new_phone[1:]
    else:
        return [False, new_phone]
    return [True, new_phone]

def standardize_phone(formated_phone):
    correct_phone_format = "8 (9xx) xxx-xx-xx"
    new_correct_phone = ""
    index = 0
    for char in correct_phone_format:
        if char == "x":
            new_correct_phone += formated_phone[index]
            index += 1
        else:
            new_correct_phone += char
    return new_correct_phone


@app.get("/unify_phone")
def open_phone_page():
    with open("templates/phone.html", "r") as f:
        phone_page = f.read()
    return Response(phone_page, media_type="text/html")


@app.post("/unify_phone_from_json")
def unify_phone(data: dict = Body(...)):
    """The service processes HTTP POST requests and accepts a phone in the request body as JSON"""
    phone = data["phone"]
    with open("templates/phone.html", "r") as f:
        phone_page = f.read()
    if not phone:
        """If there is not any phone, return phone page"""
        return Response(phone_page, media_type="text/html")
    formated_phone = check_phone(phone)
    if False in formated_phone:
        # return Response(formated_phone[1], media_type="text/html")
        return Response(
            json.dumps({
                "success": False,
                "message": formated_phone[1]
            }),
            media_type="application/json")
    else:
        correct_phone = standardize_phone(formated_phone[1])
        # return Response(correct_phone, media_type="text/html")

        return Response(
            json.dumps({
                "success": True,
                "message": correct_phone
            }),
            media_type="application/json")

# @app.post("/unify_phone_from_form")
# def unify_phone(phone: str = Form(...)):
#     """The service processes HTTP POST requests and accepts a phone in the Form Data body"""
#     with open("templates/phone.html", "r") as f:
#         phone_page = f.read()
#     if not phone:
#         """If there is not any phone, return phone page"""
#         return Response(phone_page, media_type="text/html")
#     formated_phone = check_phone(phone)
#     if False in formated_phone:
#         return Response(formated_phone[1], media_type="text/html")
#     else:
#         correct_phone = standardize_phone(formated_phone[1])
#         return Response(correct_phone, media_type="text/html")


@app.get("/unify_phone_from_query")
def unify_phone(phone):
    with open("templates/phone.html", "r") as f:
        phone_page = f.read()
    if not phone:
        """If there is not any phone, return phone page"""
        return Response(phone_page, media_type="text/html")
    formated_phone = check_phone(phone)
    if False in formated_phone:
        return Response(formated_phone[1], media_type="text/html")
    else:
        correct_phone = standardize_phone(formated_phone[1])
        return Response(correct_phone, media_type="text/html")


@app.get("/unify_phone_from_cookies")
# def unify_phone(phone):
def unify_phone(phone: Optional[str] = Cookie(default=None)):
    """The service processes HTTP GET requests and accepts the phone in the cookie record 'phone'"""
    if not phone:
        pass
    formated_phone = check_phone(phone)
    if False in formated_phone:
        response = Response(formated_phone[1], media_type="text/html")
        # response.set_cookie(key="phone", value=formated_phone[1])
        return response
    else:
        correct_phone = standardize_phone(formated_phone[1])
        response = Response(correct_phone, media_type="text/html")
        # response.set_cookie(key="phone", value=correct_phone)
        return response

@app.get("/random_page")
def open_random_wiki_page():
    with open("templates/wiki.html", "r") as f:
        random_wiki_page = f.read()
    return Response(random_wiki_page, media_type="text/html")

@app.get("/show_random_page")
def show_random_page():
    """The service finds one random page from Wikipedia and returns its name, description and url"""
    try:
        random_page_name = wikipedia.random(pages=1)
        print(random_page_name)
        random_page = wikipedia.page(random_page_name)
        print("Random page is", random_page)

        page_description = random_page.summary
        page_url = random_page.url

        return Response(
            json.dumps({
                "success": True,
                "name": random_page_name,
                "description": page_description,
                "url": page_url
            }),
            media_type="application/json")

    except wikipedia.exceptions.DisambiguationError as e:
        print("I'M IN EXCEPT")
        #Returns a list of URLs if the request refers to more than one page
        print("naen", e.options)
        try:
            print("I'M IN TRY EXCEPT")
            page_name = choice(e.options)
            print("PAGE_NAME = ", page_name)
            page = wikipedia.page(page_name)
            page_description = page.summary
            page_url = page.url

            return Response(
                json.dumps({
                    "success": True,
                    "name": page_name,
                    "description": page_description,
                    "url": page_url
                }),
                media_type="applicatoin/json")

        except:
            print("I'M IN EXCEPT EXCEPT")
            message = "Ooops, something went wrong... "
            return Response(
                json.dumps({
                    "success": False,
                    "message": message
                }),
                media_type="application/json")

    except:
        print("I'M IN FINAL EXCEPT")
        message = "Ooops, something went wrong... "
        return Response(
            json.dumps({
                "success": False,
                "message": message
            }),
            media_type="application/json")
